﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookClass.Comparators
{
    public class PriceComparer : IComparer<Book>
    {
        public int Compare(Book x, Book y)
        {
            if (x is null)
            {
                if (y is null)
                {
                    return 0;
                }

                return -1;
            }

            if (y is null)
            {
                return 1;
            }

            if (x.Price > y.Price)
            {
                return 1;
            }

            if (x.Price == y.Price)
            {
                return 0;
            }

            return -1;
        }
    }
}
