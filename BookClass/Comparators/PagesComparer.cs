﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookClass.Comparators
{
    public class PagesComparer : IComparer<Book>
    {
        public int Compare(Book x, Book y)
        {
            if (x is null)
            {
                if (y is null)
                {
                    return 0;
                }

                return -1;
            }

            if (y is null)
            {
                return 1;
            }

            if (x.Pages > y.Pages)
            {
                return 1;
            }

            if (x.Pages == y.Pages)
            {
                return 0;
            }

            return -1;
        }
    }
}
