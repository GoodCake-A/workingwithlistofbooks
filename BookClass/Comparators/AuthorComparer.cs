﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace BookClass.Comparators
{
    public class AuthorComparer : IComparer<Book>
    {
        public int Compare(Book x, Book y)
        {
            if (x is null)
            {
                if (y is null)
                {
                    return 0;
                }

                return -1;
            }

            if (y is null)
            {
                return 1;
            }

            if (string.Compare(x.Author, y.Author, StringComparison.Ordinal) > 0)
            {
                return 1;
            }

            if (string.Compare(x.Author, y.Author, StringComparison.Ordinal) == 0)
            {
                return 0;
            }

            return -1;
        }
    }
}
