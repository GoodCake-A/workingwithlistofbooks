﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using BookClass.Comparators;

namespace BookClass.StorageServices
{
    public class BookListService
    {
        private bool IsStoredRecordsChanged;

        private Lazy<List<Book>> listOfBooks;

        private List<Book> listOfAddedBooks;

        private Stream storageFileStream;

        public BookListService(IBookListStorage storageHandler)
        {
            if (storageHandler is null)
            {
                throw new ArgumentNullException(nameof(storageHandler));
            }

            this.listOfAddedBooks = new List<Book>();
            this.storageFileStream = storageHandler.GetStorageStream();

            this.listOfBooks = new Lazy<List<Book>>(() => GetBooksInStorage());
        }

        public BookListService(string pathOfFileStorage)
        {
            this.listOfAddedBooks = new List<Book>();
            this.storageFileStream = new FileStream(
                pathOfFileStorage ?? throw new ArgumentNullException(nameof(pathOfFileStorage)),
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite);

            this.listOfBooks = new Lazy<List<Book>>(() => GetBooksInStorage());
        }

        ~BookListService()
        {
            this.storageFileStream?.Close();
        }

        public void Add(Book book)
        {
            if (book is null)
            {
                throw new ArgumentNullException(nameof(book));
            }

            var listOfBooksValue = this.listOfBooks.Value;

            foreach (var element in listOfBooksValue)
            {
                if (element.CompareTo(book) == 0)
                {
                    throw new InvalidOperationException($"{nameof(book)} alredy exists in the storage.");
                }
            }

            foreach (var element in this.listOfAddedBooks)
            {
                if (element.CompareTo(book) == 0)
                {
                    throw new InvalidOperationException($"{nameof(book)} alredy exists in the storage.");
                }
            }

            this.listOfAddedBooks.Add(book);
        }

        public void Remove(Book book)
        {
            if (book is null)
            {
                throw new ArgumentNullException(nameof(book));
            }

            for (int i = 0; i < listOfBooks.Value.Count; i++)
            {
                if (this.listOfBooks.Value[i].CompareTo(book) == 0)
                {
                    this.listOfBooks.Value.RemoveAt(i);
                    this.IsStoredRecordsChanged = true;
                    return;
                }
            }

            for (int i = 0; i < listOfAddedBooks.Count; i++)
            {
                if (this.listOfAddedBooks[i].CompareTo(book) == 0)
                {
                    this.listOfAddedBooks.RemoveAt(i);
                    return;
                }
            }

            throw new InvalidOperationException($"there is no {nameof(book)} in the storage.");
        }

        public void Save()
        {
            if (!(this.storageFileStream.CanWrite && this.storageFileStream.CanSeek))
            {
                throw new InvalidOperationException($"{nameof(this.storageFileStream)} should be able to write and seek.");
            }

            var listOfBooksValue = this.listOfBooks.Value;

            var binaryFormatter = new BinaryFormatter();

            if (this.IsStoredRecordsChanged)
            {
                this.storageFileStream.Seek(0, SeekOrigin.Begin);

                foreach (var oldElement in this.listOfBooks.Value)
                {
                    binaryFormatter.Serialize(this.storageFileStream, oldElement);
                }

                this.IsStoredRecordsChanged = false;
            }
            else
            {
                this.storageFileStream.Seek(0, SeekOrigin.End);
            }

            foreach (var addedElement in this.listOfAddedBooks)
            {
                listOfBooksValue.Add(addedElement);
                binaryFormatter.Serialize(this.storageFileStream, addedElement);
            }

            this.storageFileStream.SetLength(storageFileStream.Position);

            this.listOfAddedBooks.Clear();
        }

        public void Load()
        {
            var listOfBooksValue = this.listOfBooks.Value;
        }

        public List<Book> SortBy(IComparer<Book> comparer)
        {
            var resultOfSorting = new List<Book>(this.listOfBooks.Value);

            resultOfSorting.AddRange(this.listOfAddedBooks);

            resultOfSorting.Sort(comparer);

            return resultOfSorting;
        }

        private List<Book> GetBooksInStorage()
        {
            if (!(this.storageFileStream.CanRead && this.storageFileStream.CanSeek))
            {
                throw new InvalidOperationException($"{nameof(this.storageFileStream)} should be able to read and seek.");
            }

            var listOfStoredBooks = new List<Book>();

            var binaryFormatter = new BinaryFormatter();

            this.storageFileStream.Seek(0, SeekOrigin.Begin);
            while (this.storageFileStream.Length != this.storageFileStream.Position)
            {
                listOfStoredBooks.Add((Book)binaryFormatter.Deserialize(this.storageFileStream));
            }

            return listOfStoredBooks;
        }
    }
}
