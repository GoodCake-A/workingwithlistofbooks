﻿using System.IO;

namespace BookClass.StorageServices
{
    public interface IBookListStorage
    {
        public Stream GetStorageStream();
    }
}
