﻿using System;
using System.Text;
using System.Globalization;

#pragma warning disable CA1067
#pragma warning disable CA1036

namespace BookClass
{
    [Serializable]
    public sealed class Book : IComparable, IComparable<Book>, IEquatable<Book>, IFormattable
    {
        private bool published;

        private DateTime datePublished;

        private int totalPages;

        public string Author { get; private set; }

        public string Title { get; private set; }

        public int Pages
        {
            get => totalPages;
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException($"{nameof(value)} should be positive number");
                }

                this.totalPages = value;
            }
        }

        public string Publisher { get; private set; }

        public string ISBN { get; private set; }

        public decimal Price { get; private set; }

        public string Currency { get; private set; }

        public Book(string author, string title, string publisher, string isbn)
        {
            if (String.IsNullOrWhiteSpace(author))
            {
                throw new ArgumentNullException(nameof(author));
            }

            if (String.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentNullException(nameof(title));
            }

            if (String.IsNullOrWhiteSpace(publisher))
            {
                throw new ArgumentNullException(nameof(publisher));
            }

            if (isbn is null)
            {
                throw new ArgumentNullException(nameof(isbn));
            }


            this.Author = author.Trim(' ');
            this.Title = title.Trim(' ');
            this.Publisher = publisher.Trim(' ');
            this.ISBN = new string(isbn.Length switch
            {
                0 => String.Empty,
                10 => isbn,
                13 => isbn,
                _ => throw new ArithmeticException($"{isbn} should be 10- or 13-digit number.")
            });
        }

        public Book(string author, string title, string publisher) : this(author, title, publisher, string.Empty)
        {
        }

        public void Publish(DateTime datePublished)
        {
            if (!this.published)
            {
                this.published = true;
                this.datePublished = datePublished;
            }
        }

        public void SetPrice(decimal price, string regionName)
        {
            if (String.IsNullOrWhiteSpace(regionName))
            {
                throw new ArgumentNullException(nameof(regionName));
            }

            if (price < 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(price)} cannot be less than zero.");
            }

            var info = new RegionInfo(regionName);
            this.Currency = info.CurrencySymbol;

            this.Price = price;
        }

        public override string ToString()
        {
            return this.Title + " by " + this.Author;
        }

        public int CompareTo(object obj)
        {
            if (obj is null)
            {
                if (this is null)
                {
                    return 0;
                }

                return 1;
            }

            if (!(obj is Book))
            {
                throw new ArgumentException($"{nameof(obj)} should be an instance of Book.");
            }

            Book bookRepresentation = (Book)obj;
            if (string.Compare(this.Title, bookRepresentation.Title, StringComparison.Ordinal) > 0)
            {
                return 1;
            }

            if (string.Compare(this.Title, bookRepresentation.Title, StringComparison.Ordinal) == 0)
            {
                return 0;
            }

            return -1;
        }

        public int CompareTo(Book other)
        {
            if (other is null)
            {
                if (this is null)
                {
                    return 0;
                }

                return 1;
            }

            if (string.Compare(this.Title, other.Title, StringComparison.Ordinal) > 0)
            {
                return 1;
            }

            if (string.Compare(this.Title, other.Title, StringComparison.Ordinal) == 0)
            {
                return 0;
            }

            return -1;
        }

        public bool Equals(Book other)
        {
            if (other is null)
            {
                return false;
            }

            if (this.ISBN.Equals(other.ISBN, StringComparison.Ordinal))
            {
                return true;
            }

            return false;
        }

        public string ToString(string format)
        {
            return this.ToString(format, CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format">"Y - Year, P - Publisher, I - ISBN, S - Size in pages, C - Cost"</param>
        /// <param name="formatProvider"></param>
        /// <returns></returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (format is null)
            {
                throw new ArgumentNullException(nameof(format));
            }

            if (formatProvider is null)
            {
                throw new ArgumentNullException(nameof(formatProvider));
            }

            var result = new StringBuilder(this.ToString());
            result.Append('.');

            foreach (var symbol in format)
            {
                result.Append(' ');
                result.Append(this.GetFieldAsString(symbol, formatProvider));
            }

            return result.ToString();
        }

        private string GetFieldAsString(char symbol, IFormatProvider format)
        {
            symbol = char.ToUpper(symbol, CultureInfo.CreateSpecificCulture("en-US"));

            var fieldAsString = symbol switch
            {
                'Y' => this.datePublished.Year.ToString(format) + '.',
                'P' => this.Publisher + '.',
                'I' => "ISBN: " + this.ISBN + '.',
                'S' => this.Pages.ToString(format) + " pages.",
                'C' => this.Price.ToString(format) + " " + this.Currency+'.',
                _ => throw new ArgumentException("Invalid format"),
            };

            return fieldAsString;
        }
    }
}
