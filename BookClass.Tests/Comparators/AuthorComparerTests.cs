﻿using System.Collections.Generic;
using BookClass.Comparators;
using NUnit.Framework;

#pragma warning disable SA1600
#pragma warning disable CA1707

namespace BookClass.Tests.Comparators
{
    [TestFixture]
    public static class AuthorComparerTests
    {
        [Test]
        public static void AuthorComparerSortTest()
        {
            // Arrange
            string publisher = "publisher";
            string title = "Title";
            var inputAuthorsNames = new string[] { "Bobby", "Bob", "Peter", "Andrew", "Karl" };
            var expectedAuthorsNames = new string[] { "Andrew", "Bob", "Bobby", "Karl", "Peter" };
            var bookList = new List<Book>();
            for (int i = 0; i < inputAuthorsNames.Length; i++)
            {
                bookList.Add(new Book(inputAuthorsNames[i], title, publisher));
            }

            // Act
            bookList.Sort(new AuthorComparer());

            // Assert
            int j = 0;
            foreach (var book in bookList)
            {
                Assert.AreEqual(expectedAuthorsNames[j], book.Author);
                j++;
            }
        }
    }
}
