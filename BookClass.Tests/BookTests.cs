﻿using System.Globalization;
using NUnit.Framework;

#pragma warning disable SA1600
#pragma warning disable CA1707

namespace BookClass.Tests
{
    [TestFixture]
    public class BookTests
    {
        [Test]
        public void ConstructorTest_ThreeParameters()
        {
            // Arrange
            string name = " Name";
            string author = "Author ";
            string publisher = " Publisher ";

            // Act
            var book = new Book(author, name, publisher);

            // Assert
            Assert.AreEqual("Name", book.Title);
            Assert.AreEqual("Author", book.Author);
            Assert.AreEqual("Publisher", book.Publisher);
        }

        [Test]
        public void ConstructorTest_FourParameters()
        {
            // Arrange
            string name = " Name";
            string author = "Author ";
            string publisher = " Publisher ";
            string isbn = "1234567890";

            // Act
            var book = new Book(author, name, publisher, isbn);

            // Assert
            Assert.AreEqual("Name", book.Title);
            Assert.AreEqual("Author", book.Author);
            Assert.AreEqual("Publisher", book.Publisher);
            Assert.AreEqual(isbn, book.ISBN);
        }

        [TestCase("Book Title", "Author name", "Publisher", ExpectedResult = "Book Title by Author name")]
        public string ToStringTest(string title, string author, string publisher)
        {
            var book = new Book(author, title, publisher);

            return book.ToString();
        }

        [TestCase("Book Title", "Author name", "Publisher", "9781617294532", 64, 2000, 100, "US", "ypisc", ExpectedResult = "Book Title by Author name. 2000. Publisher. ISBN: 9781617294532. 64 pages. 100 $.")]
        [TestCase("Book Title", "Author name", "Publisher", "9781617294532", 64, 2000, 100, "US", "yps", ExpectedResult = "Book Title by Author name. 2000. Publisher. 64 pages.")]
        [TestCase("Book Title", "Author name", "Publisher", "9781617294532", 64, 2000, 100, "US", "py", ExpectedResult = "Book Title by Author name. Publisher. 2000.")]
        public string ToStringFormattableTest(string title, string author, string publisher, string isbn, int pages, int year, decimal price, string region, string format)
        {
            // Arrange
            var book = new Book(author, title, publisher, isbn);
            book.Pages = pages;
            book.SetPrice(price, region);
            book.Publish(new System.DateTime(year, 1, 1));

            // Act
            var result = book.ToString(format, CultureInfo.CreateSpecificCulture("en-US"));

            // Assert
            return result;
        }

        [TestCase(22.22, "US", ExpectedResult = "22.22$")]
        [TestCase(22.22, "RU", ExpectedResult = "22.22₽")]
        [TestCase(22.22, "BY", ExpectedResult = "22.22Br")]
        public string SetPriceTest(decimal price, string region)
        {
            // Arrange
            string name = " Name";
            string author = "Author ";
            string publisher = " Publisher ";
            string isbn = "1234567890";
            var book = new Book(author, name, publisher, isbn);

            // Act
            book.SetPrice(price, region);

            // Assert
            return book.Price.ToString(CultureInfo.CreateSpecificCulture("en-US")) + book.Currency;
        }

        [TestCase("9781617294532", "9781617294532", ExpectedResult = true)]
        [TestCase("9781617294532", "2266111566", ExpectedResult = false)]
        public bool EqualsTest(string firstISBN, string secondISBN)
        {
            // Arrange
            var firstBook = new Book("Author", "Title", "Publisher", firstISBN);
            var secondBook = new Book("Author", "Title", "Publisher", secondISBN);

            // Act
            bool result = firstBook.Equals(secondBook);

            // Assert
            return result;
        }
    }
}
