﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using BookClass.StorageServices;
using Moq;
using NUnit.Framework;

#pragma warning disable SA1600
#pragma warning disable CA1707

namespace BookClass.Tests
{
    [TestFixture]
    public class BookListServiceTests
    {
        private Book book1;
        private Book book2;
        private Book book3;

        [SetUp]
        public void SetUp()
        {
            string name = " Name1";
            string author = "Author2 ";
            string publisher = " Publisher ";
            string isbn = "1234567891";

            this.book1 = new Book(author, name, publisher, isbn);

            name = " Name2";
            author = "Author1";
            publisher = " Publisher";
            isbn = "1234567892";

            this.book2 = new Book(author, name, publisher, isbn);

            name = " Name3";
            author = "Author3";
            publisher = " Publisher";
            isbn = "1234567893";

            this.book3 = new Book(author, name, publisher, isbn);

        }

        [Test]
        public void SaveTest()
        {
            using (var storageStream = new MemoryStream())
            {
                // Arrange
                var bookStorageBookMock = new Mock<IBookListStorage>();
                bookStorageBookMock.Setup(m => m.GetStorageStream()).
                    Returns(storageStream);

                var listService = new BookListService(bookStorageBookMock.Object);

                listService.Add(this.book1);
                listService.Add(this.book2);
                listService.Add(this.book3);

                // Act
                listService.Save();

                // Assert
                storageStream.Seek(0, SeekOrigin.Begin);

                var binaryFormatter = new BinaryFormatter();

                var book1Actual = (Book)binaryFormatter.Deserialize(storageStream);
                var book2Actual = (Book)binaryFormatter.Deserialize(storageStream);
                var book3Actual = (Book)binaryFormatter.Deserialize(storageStream);

                Assert.AreEqual(0, this.book1.CompareTo(book1Actual));
                Assert.AreEqual(0, this.book2.CompareTo(book2Actual));
                Assert.AreEqual(0, this.book3.CompareTo(book3Actual));
            }
        }

        [Test]
        public void SaveAndLoadTest()
        {
            using (var storageStream = new MemoryStream())
            {
                // Arrange
                var bookStorageBookMock = new Mock<IBookListStorage>();
                bookStorageBookMock.Setup(m => m.GetStorageStream()).
                    Returns(storageStream);

                var listService1 = new BookListService(bookStorageBookMock.Object);

                listService1.Add(this.book1);
                listService1.Add(this.book2);
                listService1.Add(this.book3);
                listService1.Save();

                var listService2 = new BookListService(bookStorageBookMock.Object);

                // Act
                listService2.Load();
                listService2.Save();

                // Assert
                storageStream.Seek(0, SeekOrigin.Begin);

                var binaryFormatter = new BinaryFormatter();

                var book1Actual = (Book)binaryFormatter.Deserialize(storageStream);
                var book2Actual = (Book)binaryFormatter.Deserialize(storageStream);
                var book3Actual = (Book)binaryFormatter.Deserialize(storageStream);

                Assert.AreEqual(0, this.book1.CompareTo(book1Actual));
                Assert.AreEqual(0, this.book2.CompareTo(book2Actual));
                Assert.AreEqual(0, this.book3.CompareTo(book3Actual));
            }
        }

        [Test]
        public void RemoveAndSaveTest()
        {
            using (var storageStream = new MemoryStream())
            {
                // Arrange
                var bookStorageBookMock = new Mock<IBookListStorage>();
                bookStorageBookMock.Setup(m => m.GetStorageStream()).
                    Returns(storageStream);

                var listService = new BookListService(bookStorageBookMock.Object);

                listService.Add(this.book1);
                listService.Add(this.book2);
                listService.Add(this.book3);
                listService.Save();

                // Act
                listService.Remove(this.book1);
                listService.Save();

                // Assert
                storageStream.Seek(0, SeekOrigin.Begin);

                var binaryFormatter = new BinaryFormatter();

                var book1Actual = (Book)binaryFormatter.Deserialize(storageStream);
                var book2Actual = (Book)binaryFormatter.Deserialize(storageStream);

                Assert.IsTrue(storageStream.Length == storageStream.Position);

                Assert.AreEqual(0, this.book2.CompareTo(book1Actual));
                Assert.AreEqual(0, this.book3.CompareTo(book2Actual));
            }
        }
    }
}
